const UserSchema = require("../Model/UserSchema");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
SECRET_KEY = "RESTAURENTAPI";
const Session = require("../Model/SessionSchema");
const os = require('os');

const userRegister = async (req, res) => {
  const { name, age, gender, email } = req.body;
  try {
    //check for existing user in db
    const exitsingUser = await UserSchema.findOne({ email: email });
    if (exitsingUser) {
      return res.status(400).json({ message: "User already exists !!!" });
    }

    //hash password
    const hashedPassword = await bcrypt.hash(req.body.password, 12);

    //create new user
    const userCreate = await UserSchema.create({
      name: name,
      age: age,
      gender: gender,
      email: email,
      password: hashedPassword,
    });

    //create token
    const token = jwt.sign(
      { email: userCreate.email, id: userCreate._id },
      SECRET_KEY      
    );

    const {password, ...rest} = userCreate._doc
    const displayData = {...rest, token};
    res.status(201).json(displayData);
  } catch (error) {
    console.log(error);
    res.status(404).json({ message: "Something Went Wrong !!!" });
  }
};


const userLogin = async (req, res) => {
  const { email } = req.body;

  try {
    // Check for if user is registered or not
    const existingUser = await UserSchema.findOne({ email: email });
    if (!existingUser) {
      return res.status(500).json({ message: "User Not Found !!!" });
    }

    // Match password with the existing user's password in the database
    const matchPassword = await bcrypt.compare(req.body.password, existingUser.password);

    if (!matchPassword) {
      return res.status(400).json({ message: "Invalid Credentials !!!" });
    }

    // Check for active session
    const activeSession = await Session.findOne({ userId: existingUser._id });
    if (activeSession) {
      // Invalidate previous session
      await Session.findByIdAndDelete(activeSession._id);
    }

    // Create a new token
    const token = jwt.sign(
      { email: existingUser.email, id: existingUser._id },
      SECRET_KEY,
      { expiresIn: "1h" }
    );

    // Store session in the database
    await Session.create({ userId: existingUser._id, token ,os_module:os.platform()});
   
    // req.session.user = {existingUser,is_login:true};

    // console.log(req.session.user);



    const { password, ...rest } = existingUser._doc;
    const displayData = { ...rest, token };

    res.status(201).json(displayData);
  } catch (error) {
    console.log(error);
    res.status(404).json({ message: "Something Went Wrong !!!" });
  }
};

module.exports = {
  userRegister,
  userLogin,
};


