const FoodSchema = require('../Model/FoodSchema.js');

const PostFoodDetails = async(req,res)=>{

    const fooddata = new FoodSchema({
        foodname: req.body.foodname,
        rest_id: req.body.rest_id,
        foodprice: req.body.foodprice,
        fooddescription:req.body.fooddescription,
    });

    try {
        await fooddata.save();
        res.status(200).json(fooddata)
    } catch (error) {
        res.status(400).json({ message: error.message})
    }
}

const GetFoodDetails = async(req,res)=>{
    
    try{
        const fooddata = await FoodSchema.find()
        res.status(200).json(fooddata)
    }catch(error){
        res.status(400).json({ message: error.message})
    }
}
const PutFoodDetails = async(req,res)=>{

    try{
            
        const food = req.body 
        const {id} = req.params

        await FoodSchema.findByIdAndUpdate(id,food).then(()=>{
            res.send('Updated Successfully!');
        })   
    }catch (error) {
        res.status(400).json({ message: error.message})
    }

}

const DeleteFoodDetails = async(req,res)=>{
    try{
        const food = req.body
        const {id} = req.params

       await FoodSchema.findByIdAndDelete(id,food).then(()=>{
            res.send('Deleted Successfully!');
        })
    }catch (error) {
        res.status(400).json({ message: error.message})
    }
}

module.exports = {PostFoodDetails,GetFoodDetails,PutFoodDetails,DeleteFoodDetails}