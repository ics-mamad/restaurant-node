const FoodSchema = require("../Model/FoodSchema.js");
const RestaurentSchema = require("../Model/RestaurentSchema.js");

const PostDetails = async (req, res) => {
  const restdata = new RestaurentSchema({
    rest_name: req.body.rest_name,
    rest_address: req.body.rest_address,
    rest_city: req.body.rest_city,
  });

  try {
    await restdata.save();
    res.status(200).json(restdata);
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
};

const GetDetails = async (req, res) => {
  try {
    const getrestdata = await RestaurentSchema.find();
    res.status(200).json(getrestdata);
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
};

const PutDetails = async (req, res) => {
  try {
    const restaurent = req.body; //it's for updating all fields
    const { id } = req.params;

    RestaurentSchema.findByIdAndUpdate(id, restaurent).then(() => {
      res.send("Updated Successfully!");
    });
  } catch {
    console.error(err.message);
    res.send(400).send("Server Error");
  }
};

const DeleteDetails = async (req, res) => {
  try {
    const restaurent = req.body;
    const { id } = req.params;

    const isDelete = await RestaurentSchema.findByIdAndDelete(id, restaurent);
    if (isDelete) {
      await FoodSchema.deleteMany({ rest_id: id }); 
    }
    res.send("Deleted Successfully Successfully!");
  } catch {
    console.error(err.message);
    res.send(400).send("Server Error");
  }
};

module.exports = { PostDetails, GetDetails, PutDetails, DeleteDetails };
