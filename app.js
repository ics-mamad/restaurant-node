const express = require('express');
const mongoose = require('mongoose');
const routes = require('./Routes/RestaurentRoute');
const userRouter = require('./Routes/UserRoute')
// session = require('express-session');

const app1=express();

const PORT = 8080

app1.use(express.json());

// app1.use(session({
//     is_login: false,
//     secret: 'SECRETAPI',
//     resave: true,
//     saveUninitialized: true
// }))

mongoose.connect('mongodb+srv://alyaniics:3FynHm8fw7IFsn4a@restaurent-app.licyad0.mongodb.net/Restaurent-App?retryWrites=true&w=majority')
.then(() => console.log("MongoDB connected..."))
.catch(err => console.log(err))

app1.use("/api", routes);
app1.use("/auth", userRouter);

app1.listen(PORT,()=>{
    console.log(`Listing At Port: ${PORT}`)
})
