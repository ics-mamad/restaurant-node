const yup = require("yup");

const restValidation = yup.object({
  rest_name: yup.string().min(5).max(15).required(),
  rest_address: yup.string().min(10).required(),
  rest_city: yup.string().required(),
});

const restUpdateValidation = yup.object({
  rest_name: yup.string().min(5).max(15),
  rest_address: yup.string().min(10),
  rest_city: yup.string(),
});

module.exports = {
  restValidation,
  restUpdateValidation,
};
