const yup = require("yup");

const foodValidation = yup.object({
    foodname: yup.string().required().min(3).max(15),
    foodprice: yup.number().required(),
    fooddescription: yup.string().required().min(15),
    rest_id: yup.string().required(),
})

const foodUpdateValidation = yup.object({
    foodname: yup.string().min(3).max(15),
    foodprice: yup.number(),
    fooddescription: yup.string().min(15),
    rest_id: yup.string(),
}
)

exports.foodValidation = {foodValidation, foodUpdateValidation};
  