const yup = require("yup");

const registerValidation = yup.object({
    name: yup.string().required(),
    age: yup.number().min(12).required(),
    gender: yup.string().required(),
    email: yup.string().email().required(),
    password: yup.string().min(6).required(),
})

module.exports = registerValidation;