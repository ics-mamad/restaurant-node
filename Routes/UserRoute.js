const express = require('express');
const {userLogin,userRegister} = require('../Controllers/UserController');
const validationMiddleware = require('../Middlewares/validationMid');
const loginValidation = require('../Validations/loginValidation');
const registerValidation = require('../Validations/registerValidation');
const userRouter = express.Router();



userRouter.post('/register',validationMiddleware(registerValidation),userRegister);
userRouter.post('/login',validationMiddleware(loginValidation), userLogin);

module.exports = userRouter;