const { Router } = require("express");
const {GetDetails,PostDetails,PutDetails,DeleteDetails,} = require("../Controllers/RestaurentController");
const auth = require("../Middlewares/auth");

const {GetFoodDetails,PostFoodDetails,PutFoodDetails,DeleteFoodDetails,} = require("../Controllers/FoodController");
const validationMid = require("../Middlewares/validationMid");
const {restValidation,restUpdateValidation} = require("../Validations/restValidation");
const {foodValidation,foodUpdateValidation} = require("../Validations/foodValidation");

const router = Router();

router.route("/restaurent").post(auth,validationMid(restValidation), PostDetails).get(auth,GetDetails);

router
  .route("/restaurent/:id")
  .put(auth,validationMid(restUpdateValidation),PutDetails)
  .delete(DeleteDetails);

router.route("/food").post(auth,validationMid(foodValidation),PostFoodDetails).get(auth,GetFoodDetails);

router
  .route("/food/:id")
  .put(auth,validationMid(foodUpdateValidation),PutFoodDetails)
  .delete(DeleteFoodDetails);

module.exports = router;
