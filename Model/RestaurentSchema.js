const mongoose  = require('mongoose');

const restaurentSchema = new mongoose.Schema({
    
    rest_name: {
        type: String,
        required: true
    },
    rest_address: {
        type: String,
        required: true
    },
    rest_city: {
        type: String,
        required: true
    },
},
{
    timestamps: true
})

module.exports = mongoose.model('Restaurent', restaurentSchema);