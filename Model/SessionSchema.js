const mongoose = require('mongoose');

const SessionSchema = new mongoose.Schema({
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    token: {
        type: String,
        required: true
    },
    os_module:{
        type: String,
        required: true
    }
}, {
    timestamps: true
});

module.exports = mongoose.model('Session', SessionSchema);
