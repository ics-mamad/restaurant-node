const mongoose = require('mongoose');

const FoodSchema = new mongoose.Schema({
    
    foodname:{
        type:String,
        required:true
    },
    rest_id:{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Restaurent',
        required: true,
    },
    foodprice:{
        type:Number,
        required:true
    },
    fooddescription:{
        type:String,
        required:true
    }
})

module.exports = mongoose.model('Food',FoodSchema)